from tkinter import *

class Paint:
    def __init__(self,root):
        self.root = root
        self.root.title("Draw a Graph")
        self.root.geometry("840x580")
        self.root.configure(background="black")
        self.root.resizable(0,0)

        ### Setup Page to Draw Graph
        self.canvas = Canvas(self.root, bg="black", bd=1, relief=GROOVE, height=560, width=700)
        self.canvas.place(x=125,y=8)

        ### Setup Node
        self.node_name = Entry(self.root, text="Name of Node", width=15,bd=1, bg="green")
        self.node_name.pack()
        self.node_name.place(x=2,y=10)

        self.add_node_button = Button(self.root, text="Add Node", bd=1, bg="green", relief=RIDGE, width=12, command=self.a)
        self.add_node_button.pack()
        self.add_node_button.place(x=2,y=50)

        ### Setup Edge
        self.edge_name = Entry(self.root, text="Name of Edge", width=15,bd=1, bg="green")
        self.edge_name.pack()
        self.edge_name.place(x=2,y=100)

        self.add_edge_button = Button(self.root, text="Add Edge", bd=1, bg="green", relief=RIDGE, width=12, command=self.b)
        self.add_edge_button.pack()
        self.add_edge_button.place(x=2,y=140)

    def a(self):
        while True:
            print("A")

    def b(self):
        while True:
            print("B")

root = Tk()
pt = Paint(root)
root.mainloop()
