from django.db import models
from django.utils import timezone

class Licenceplates(models.Model):
    created_at = models.DateField(default=timezone.now)
    username = models.CharField(max_length=100)
    image = models.ImageField(upload_to='Cars')
    location = models.CharField(max_length=100)
    licence = models.CharField(max_length=100, blank=True, null=True)