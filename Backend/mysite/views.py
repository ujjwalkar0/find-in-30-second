from django.shortcuts import render, redirect
from django.views.generic import ListView
from .models import Licenceplates
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth import authenticate, login, logout

def logout_view(request):
    logout(request)
    return render(request, "index.html")
    
class Home(ListView):
    template_name = 'index.html'

    def post(self,request):
        # try:
        username  = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(username=username, password=password)
        login(request,user)
        return render(request,'index.html',{"object_list":Licenceplates.objects.all()})

        # except MultiValueDictKeyError:
        #     if (request.POST['choice']=='created_at'):
        #         return render(request,'index.html',{"object_list":Licenceplates.objects.filter(created_at=request.POST['name'])})
        #     elif (request.POST['choice']=='username'):
        #         return render(request,'index.html',{"object_list":Licenceplates.objects.filter(username=request.POST['name'])})
        #     elif (request.POST['choice']=='location'):
        #         return render(request,'index.html',{"object_list":Licenceplates.objects.filter(location=request.POST['name'])})
        #     elif (request.POST['choice']=='licence'):
        #         return render(request,'index.html',{"object_list":Licenceplates.objects.filter(licence=request.POST['name'])})

    def get_queryset(self):
        return Licenceplates.objects.order_by('-created_at')

class Server(APIView):
    def post(self,request):
        data=request.data
        username=data["username"]
        password=data["password"]
        user = authenticate(username=username, password=password)
        if user is not None:
            location=data["location"]
            image=data["image"]
            print(data)
            Licenceplates.objects.create(username=username,location=location,image=image)
            return Response({"Msg":"Uploaded"})
        else:
            return Response({"Msg":"Authentication Failed"})
