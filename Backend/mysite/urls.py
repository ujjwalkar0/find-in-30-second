from django.urls import path
from .views import *

urlpatterns = [
    path('',Home.as_view(),name="home" ),
    path('post',Server.as_view(),name="post" ),
    path('logout',logout_view,name="logout")
]
