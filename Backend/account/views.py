from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import authenticate
from knox.models import AuthToken

class Login(APIView):
    def post(self,request,format=None):
        data = request.data
        username=data["username"]
        password=data["password"]
        user = authenticate(username=username,password=password)
        return Response({"token": AuthToken.objects.create(user)[1]})

class Register(APIView):
    def post(self, request, format=None):
        data = request.data
        first_name=data["first_name"]
        last_name=data["last_name"]
        username=data["username"]
        password=data["password"]
        re_password=data["re_password"]

        if password != re_password:
            return Response({"error":"Confirm password should be same as Entered Password"})
        
        if len(password) < 8:
            return Response({"error":"Password must have atleast 8 character"})

        if User.objects.filter(username=username).exists():
            return Response({"error":"Username Already Exist"})

        user = User.objects.create_user(
            first_name = first_name,
            last_name = last_name,
            username = username,
            password = password
        )
        user.save()
        if User.objects.filter(username=username).exists():
            return Response({"messages":"User Created"})

        return Response({"error":"Something wrong"})



        # serializer = SnippetSerializer(data=request.data)
        # if serializer.is_valid():
        #     serializer.save()
        #     return Response(serializer.data, status=status.HTTP_201_CREATED)
        # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)