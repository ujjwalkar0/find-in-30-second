# from rest_framework_jwt.views import obtain_jwt_token
from .views import *
from django.urls import path

urlpatterns = [
    # path('token/', obtain_jwt_token)
    path('register/', Register.as_view(),name="register"),
    path('login/', Login.as_view(),name="login")
]